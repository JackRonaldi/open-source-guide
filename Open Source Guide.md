**All section of this document are a Work in Progress**

## Open Source Software (OSS) Challenges

#### 1\. **Inbound Requests Volume**

- **Community Perception**
  - Misconception: OSS is community-driven
  - Reality: Dominated by few individuals
  - Role of maintainers: Direct traffic and build communities
- **Contributor Management**
  - Initiatives encouraging contributions increase workload for maintainers
  - High contributor turnover; many contribute only once
  - Need for infrastructure to foster community
- **Interaction Management**
  - Managing frequent, low-touch interactions
  - Seeking quality over quantity in contributions

#### 2\. **Funding**

- WIP

#### 3\. **Poor Contributions**

- **Skill Gap**
  - 80% unable to resolve merge conflicts
  - Potential solution: Educational tools in OSS readme
- **Question Management**
  - Preempting general questions to handle increased interest

## Considerations for OSS

#### **GitHub's Role**

- **Pros**
  - User-friendly, reliable, facilitates large-scale interactions
  - Enhances visibility and trust through detailed profiles
  - Helps creators build an audience through improved distribution, and reduced costs
- **Cons**
  - Centralized power with Microsoft
  - Potentially prioritizes convenience over openness
- **Impact**
  - Facilitated the amateurization of OSS
  - Encouraged code sharing and discovery, albeit leading to a lot of the challenges listed above

#### **Future of OSS**

- **Licensing**
  - Prior focus: Software distribution and consumption
  - Future challenge: how is software produced?
- **Licenses (WIP)**
  - GPL
  - Copyleft
  - BSD
  - MIT License (most popular, used in 45% of OSS projects)

## OSS Project Structure

#### **Community Engagement**

- **Entry Barrier**
  - Intimidating for newcomers
  - Think of ways to make the entry less daunting, while avoiding low quality contributions, spam and unengaged contributors
- **Communication Channels**
  - Various platforms for synchronous and asynchronous communication
    * Synchronous: IRC, Discord, Slack, Matrix
    * Asynchronous: Email, Reddit, StackOverflow
  - Used for coordination, community building, education, and office hours

#### **Project Stages**

- **Creation**
  - Closed development phase with limited developers
- **Evangelism**
  - Open development phase focusing on feedback and distribution
- **Growth**
  - **What Happens?**
    - The project gets used by more people.
    - Maintainers spend more time helping users than working on the project.
  - **Challenges**
    - Too many different ways people use the project.
    - Too much noise in community chats.
  - **Potential Solutions**
    - Asking more people to help out and share the work.

#### **Types of Projects**

- **Federations**
  - **When:** High Contributor & User Growth
  - **Example:** Rust
  - **Like:** Big companies or NGOs
  - **How it Works:**
    - Lots of people help in different small groups specialized in certain areas of the project.
    - They use voting and have leaders to make decisions.
    - Often have foundations to manage
  - **Challenges:** 
    - Poor contributions from new contributors
    - A lot of user interactions to manage
    - Sometimes small groups face the same problems the big group had before
  - ** Potential Solutions:**
    - Reduce friction to contribute to maintain high contributor growth rate
    - Avoid big decision hierarchies
    - Rely on users to support each other, or new contributors to support users.

- **Clubs**
  - **When:** High Contributor Growth & Low User Growth
  - **Example:** Astropy
  - **Like:** A hobby group where everyone shares the same interest.
  - **How it Works:**
    - The project lasts as long as a small group keeps working on it.
    - It's important who joins because it's a small group.
    - Highly sticky, retaining large portion of contributors.

- **Stadiums**
  - **When:** Low Contributor Growth & High User Growth
  - **Example:** Babel
  - **Like:** The creator economy, users face the stage.
  - **How it Works:**
    - A few people make decisions for many users.
    - Hard for new people to help with big tasks.
    - Many widely depended on packages and libraries are stadiums.
  - **Challenges:**
    - Few maintainers become inundated with managing community interactions.
    - Expensive to onboard new maintainers, because requires knowledge that isn't easily picked up by others.
  - **Potential Solutions:**
    - Rely more heavily on automation, user to user support and aggressive elimination of noise.
    - Make it easier for new people to help so it can become a federation.

- **Toys**
  - **When:** Low Contributor & User Growth
  - **Example:** ssh-chat
  - **Like:** Personal projects just for fun.

## Roles, Incentives & Relationships

#### **Theory of the Commons**

- **What is it?**
  - A set of rules that help a community work well together.
- **Rules Include:**
  - Clear membership: knowing who is in the group.
  - Matching rules to actual conditions.
  - Letting affected people change the rules.
  - The community, or people accountable to them monitor adherence to the rules.
  - Having fair punishments for rule-breakers, with graduated sanctions depending on seriousness and context of offence.
  - Solving conflicts in the community without spending a lot.
  - External authorities respect the community's decisions.
  - Having different levels of power.
- **Why it Works:**
  - People trust each other and want to work together for a long time.
  - People work because they want to, not for money.
  - Works more so for decentralized communities.
    - These communities prioritize work based on abundance of attention, encouraging new contributors, developing governance processes and improving engagement and retention.

#### **Necessary Conditions for Community Based Peer Production**

- **Intrinsic Motivation**
  - Members do the work because they want to.
  - Some think money shouldn't be involved.
  - The community is strong because people help out of desire, not for money.
- **Working Together Easily:**
  - People can quickly find tasks they want to do.
  - Tasks are small and easy to handle.
- **Challenges:**
  - It's hard to check the quality of work.
  - Not everyone likes reviewing others' work.

#### **What about Stadiums?**

- **How it works:**
  - Centralized community
  - Prioritize work based on scarcity of attention.
    - Saying no to contributions, closing out issues, reducing user support (curation)
  - GitHub dropped the barrier to contributing to an unfamiliar project, allowing people to move between communities and making collective identity less defined.

#### **Changes in Communities**
- **Before and Now**
  - **Before GitHub**: Each project was a small community where everyone knew each other.
  - **Now with GitHub**: It's easy to help in different projects, but people don't know each other as well.
- **Problems**
  - **Less Unity**: People help casually and don't feel strongly connected to one community.
  - **Trolls**: Some people misbehave and it's hard to stop them because they don't care about the community.
- **Big Question**
  - **Who Belongs?**: It's tough to decide who is really a part of the community.

#### **Rules in the Community**
- **Code of Conduct**
  - **Why**: To keep the community safe and respectful.
  - **Who Decides**: It's unclear who has the power to set the rules and punish those who break them.
- **Rough Consensus**
  - **What**: A way to make decisions without voting, but by discussing until no one strongly disagrees.
  - **Where it Works Best**: In big communities where people feel a strong connection to the group and there is a bigger contributor community (e.g. federations and clubs). For centralized communities like stadiums, they are better suited to BDFL than consensus seeking.

### Roles in Open Source Projects

#### **1. Maintainers**
- **Who They Are**
  - **Trustees**: They take care of the project's future and make big decisions.
  - **Different Roles**: Some write new things, while others take care of what's already there.
- **Responsibilities**
  - Curation: Filtering relevant information from noise
  - Balancing community interactions and code writing
  - In some cases, acting as a "Benevolent Dictator for Life (BDFL)"
- **Challenges**
  - **Hard to Find**: Not many people want this job because it often becomes more about taking care of what's there than creating new things.
  - **Burnout**: They can get tired if they don't enjoy the job or can't share the work with others.
- **Big Questions**
  - **Rewards**: How to keep them happy and motivated?
  - **Finding New Ones**: How to find new maintainers when the old ones leave?

#### **2. Contributors**
- **Who They Are**
  - **Helpers**: They help in different ways, from writing code to fixing small errors or speaking at events.
  - **Active and Casual**: Some help a lot and regularly, while others help once in a while.
- **Challenges**
  - **Recognition**: How to recognize different kinds of help?
  - **Responsibility**: Some don't want to be recognized to avoid extra responsibilities.
- **Big Questions**
  - **Encouraging More Helpers**: How to make more people want to help regularly?
  - **Meaningful Contributions**: How to create a space where everyone feels their help matters?

#### **3. Users**
- **Who They Are**
  - **Silent Majority**: They use the code but don't see themselves as contributors.
  - **Unknown to Maintainers**: Maintainers often don't know who their users are, but sense the project's popularity through issues or pull requests.
  - **Unaware Users**: Some developers might use a project without realizing it, especially if it's a dependency.
- **Active Users**
  - **Contributions**: Some users contribute in various ways like education, spreading the word, support, and bug reports.
  - **Traits**: They show traits of both active and casual contributors. They might work away from the main community and may never have interacted with the project repository.
  - **Best Friend or Foe**: They can be very helpful or very demanding, depending on their approach.
  
### **Assessing Project Health**
- **Metrics**
  - **Popularity**: How many people use the project.
  - **Dependency**: How much other software relies on the project.
  - **Activity**: Is the project actively developed?
- **GitHub Archive**: A place where anyone can download and fork a project, but interactions are turned off.
- **Bus Factor**: A measure of a project's resilience based on how many people would need to be "hit by a bus" for the project to fail.
- **Contributors**: While they are commonly used to measure project health, it can be misleading, especially for stadiums.
- **Work Metrics**: It's essential to look at the volume of work required, maintainer responsiveness, and other activity metrics to get a holistic view of project health.

### **Software Maintenance**
- **Software Longevity**
  - **Never Truly Finished**: Software requires ongoing maintenance, even if it seems feature-complete.
  - **Rewriting Benefits**: It can reduce complexity and transfer knowledge to new team members.
  - **Software's Stubbornness**: Once software finds users, it's hard for it to disappear completely.
- **Active vs. Static State Code**
  - **Static State**: Code that is published and can be viewed but isn't used in production.
  - **Active State**: Code that is being used and depends on other things. It incurs hidden costs over time.
- **Open Source is Free, Like a Puppy**
  - **Ongoing Care**: Just like a puppy needs care and feeding, active state code requires continuous maintenance.
  - **Freedom to Fork**: This concept only works when ignoring the dependencies and upkeep required by a project.
- **Hidden Costs of Software**
  - **Creation, Distribution, and Maintenance**: While creation might be powered by intrinsic motivation and distribution might be low-cost, maintenance can be expensive and time-consuming.
- **JavaScript's Popularity**: With its rise in web development, the maintenance costs and challenges associated with backward compatibility become crucial.
- **Software's Resilience**: Software, especially older languages like Fortran and COBOL, can remain in use for a long time due to their stability and the industries they serve.
- **Maintenance Challenges**: The costs of maintenance and the lack of intrinsic motivation to maintain are why large open source projects become modular as they grow.

**Cornucopia of the Commons**
- Concept: More value is created as more people use the resource and join the social community.
- Example: Phone network.
- Open source code is often treated as a public good.
- Software is not entirely non-rivalrous.
    - As more users access software, it can impact the experience for subsequent users.
    - Difference in experience between 10 vs. 10,000 users.

**Physical Infrastructure**
- Software requires infrastructure to serve a large audience reliably.
    - Costs are real, even if absorbed by central providers.
    - Hosting costs are reduced for creators, but not eliminated.
    - Marginal costs can become significant at scale.
    - Examples of challenges: DDoS attacks, serving millions of users

**User Support**
- As adoption grows, support costs can increase.
    - Questions and bug reports increase with more users.
    - Asymmetry between community participation and the burden on community leaders.

**Community Moderation**
- Negative behavior can deter contributions.
    - 1/5 of people stop contributing due to negative experiences.

**Temporal Costs**
- Software requires ongoing maintenance.
    - Code decay is inevitable.
    - Documentation must be updated with code changes.
    - Technical debt accumulates over time.
        - Refactoring is essential but can be challenging.
    - Dependency management is crucial.
        - Dependencies can become outdated or incompatible.
        - Security vulnerabilities in dependencies pose risks.
    - Adapting to user needs:
        - Maintenance is reactive, but user needs evolve.
        - Innovator’s dilemma: Balancing maintenance with innovation.
        - Deciding when to support or deprecate software is challenging.

**Measuring the Value of Code**
- The concept of zero marginal cost.
    - Developers have access to vast amounts of code.
    - Reusing existing software components is cost-effective.

## Miscellaneous Notes

#### **Developer Engagement**

- Predominantly passive consumption rather than active contribution

#### **Code Trends**

- Increasing modularity with small libraries
- Issues with code attribution and understanding origin

## **Ideas**

- **Learning from npm**
  - Understanding the functioning of npm, used by 97% of web apps, to glean insights for OSS
- **Commit Access**
  - Different processes and considerations for granting commit access based on project needs
- **Change Approval**
  - Varies based on complexity and the proposer's reputation
  - Encouraging formal channels over personal outreach for reviews
